import 'package:flaconi_task/api_models/weather.dart';
import 'package:test/test.dart';

void main() {
  group('Weather', () {
    group('fromJson', () {
      test('returns correct Weather object', () {
        expect(
          Weather.fromJson(
            const <String, dynamic>{
              'temp': 10.3,
              'feels_like': 12.1,
              'humidity': 30,
              'pressure': 900,
            },
          ),
          isA<Weather>()
              .having((w) => w.humidity, 'humidity', 30)
              .having((w) => w.pressure, 'pressure', 900)
              .having((w) => w.temp, 'temp', 10.3)
              .having((w) => w.feelsLike, 'feels_like', 12.1),
        );
      });
    });
  });
}
