import 'dart:convert';

import 'package:flaconi_task/api_models/weather.dart';
import 'package:flaconi_task/api_models/weather_condition.dart';
import 'package:flaconi_task/api_models/weather_data.dart';
import 'package:flaconi_task/api_models/weather_wind.dart';
import 'package:test/test.dart';

void main() {
  group('WeatherData', () {
    group('fromJson', () {
      test('returns correct WeatherData object', () {
        expect(
          WeatherData.fromJson(
            <String, dynamic>{
              'main': const Weather(
                      temp: 12.1, feelsLike: 1.1, humidity: 30, pressure: 901)
                  .toJson(),
              'weather': [
                const WeatherCondition(icon: '10n', main: 'Cloudy').toJson()
              ],
              'wind': const WeatherWind(speed: 12.1).toJson(),
              'dt_txt': DateTime.parse('2024-03-10 00:00:00').toString(),
            },
          ),
          isA<WeatherData>()
              .having(
                (w) => w.main.toJson(),
                'main',
                const Weather(
                        temp: 12.1, feelsLike: 1.1, humidity: 30, pressure: 901)
                    .toJson(),
              )
              .having(
                (w) => jsonEncode(w.weather).toString(),
                'weather',
                jsonEncode([
                  const WeatherCondition(icon: '10n', main: 'Cloudy').toJson()
                ]).toString(),
              )
              .having(
                (w) => w.wind.toJson(),
                'wind',
                const WeatherWind(speed: 12.1).toJson(),
              )
              .having(
                (w) => w.dateTime.toString(),
                'dateTime',
                DateTime.parse('2024-03-10 00:00:00').toString(),
              ),
        );
      });
    });
  });
}
