import 'package:bloc_test/bloc_test.dart';
import 'package:flaconi_task/cubit/weather_cubit.dart';
import 'package:flaconi_task/models/weather.dart';
import 'package:flaconi_task/models/weather_forecast.dart';
import 'package:flaconi_task/repository/weather_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'helpers.dart';

class MockWeatherRepository extends Mock implements WeatherRepository {}

class MockWeather extends Mock implements Weather {}

class MockWeatherCubit extends Mock implements WeatherCubit {}

class MockWeatherForecast extends Mock implements WeatherForecast {}

const weatherCondition = 'Rainy';
const weatherTemperature = 9.8;
const weatherFeelsTemperature = 8.8;
const windSpeed = 8;
const pressure = 1008;
const humidity = 25;

void main() {
  initHydratedStorage();
  group('WeatherCubit', () {
    late WeatherCubit weatherCubit;

    late WeatherForecast forecast;
    late Weather weather;
    late WeatherRepository weatherRepository;
    late DateTime fakeDate;
    setUp(() async {
      final testForecast = [
        Weather(
          temp: Temperature(value: weatherTemperature),
          feelsLike: Temperature(value: weatherFeelsTemperature),
          humidity: humidity,
          pressure: pressure,
          windSpeed: windSpeed,
          icon: '10n',
          date: DateTime.parse('2024-03-10 00:00:00'),
          weatherCondition: weatherCondition,
        ),
        Weather(
          temp: Temperature(value: weatherTemperature),
          feelsLike: Temperature(value: weatherFeelsTemperature),
          humidity: humidity,
          pressure: pressure,
          windSpeed: windSpeed,
          icon: '10n',
          date: DateTime.parse('2024-03-10 03:00:00'),
          weatherCondition: weatherCondition,
        ),
      ];

      fakeDate = DateTime.parse('2024-03-10 00:00:00');
      weather = MockWeather();
      forecast = MockWeatherForecast();
      weatherRepository = MockWeatherRepository();
      when(() => weather.date.toString()).thenReturn(fakeDate.toString());
      when(() => weather.weatherCondition).thenReturn(weatherCondition);
      when(() => weather.windSpeed).thenReturn(11);
      when(() => weather.feelsLike)
          .thenReturn(Temperature(value: weatherFeelsTemperature));
      when(() => weather.temp)
          .thenReturn(Temperature(value: weatherTemperature));
      when(() => weather.pressure).thenReturn(1333);
      when(() => weather.icon).thenReturn('10n');

      when(() => forecast.forecast).thenReturn(testForecast);

      when(
        () => weatherRepository.getForecast(),
      ).thenAnswer((_) async => forecast);

      weatherCubit = WeatherCubit(weatherRepository);
    });

    test('initial state is correct', () {
      final weatherCubit = WeatherCubit(weatherRepository);
      expect(weatherCubit.state, WeatherState());
    });

    group('fetchWeather', () {
      blocTest<WeatherCubit, WeatherState>(
        'calls getWeather',
        build: () => weatherCubit,
        act: (cubit) => cubit.fetchWeather(),
        verify: (_) => verify(() => weatherRepository.getForecast()).called(1),
      );

      blocTest<WeatherCubit, WeatherState>(
        'emits [loading, failure] when getWeather throws',
        setUp: () {
          when(
            () => weatherRepository.getForecast(),
          ).thenThrow(Exception('oops'));
        },
        build: () => weatherCubit,
        act: (cubit) => cubit.fetchWeather(),
        expect: () => <WeatherState>[
          WeatherState(status: WeatherStatus.loading),
          WeatherState(status: WeatherStatus.failure),
        ],
      );

      blocTest<WeatherCubit, WeatherState>(
        'emits [loading, success] when getWeather returns (celsius)',
        build: () => weatherCubit,
        act: (cubit) => cubit.fetchWeather(),
        expect: () => <dynamic>[
          WeatherState(status: WeatherStatus.loading),
          isA<WeatherState>()
              .having((w) => w.status, 'status', WeatherStatus.success)
              .having(
                  (w) => w.weather,
                  'weather',
                  isA<Weather>()
                      .having((w) => w.temp, 'temp', isNotNull)
                      .having((w) => w.weatherCondition, 'condition',
                          weatherCondition)
                      .having((w) => w.temp, 'temperature',
                          Temperature(value: weatherTemperature))
                      .having((w) => w.feelsLike, 'temperature feels like',
                          Temperature(value: weatherFeelsTemperature))
                      .having((w) => w.windSpeed, 'wind', windSpeed)
                      .having((w) => w.pressure, 'pressure', pressure)
                      .having((w) => w.humidity, 'humidity', humidity)),
        ],
      );
    });

    test('refreshWeather calls fetchWeather', () async {
      await weatherCubit.refreshWeather();
      verify(weatherRepository.getForecast).called(1);
    });
    //

    test('toggleUnits emits correct states', () {
      final initialWeatherState = WeatherState(
        status: WeatherStatus.success,
        weather: Weather(
          temp: Temperature(value: 25),
          feelsLike: Temperature(value: 27),
          humidity: 60,
          pressure: 1013,
          windSpeed: 10,
          icon: '10n',
          date: DateTime.parse('2024-03-10 00:00:00'),
          weatherCondition: 'Cloudy',
        ),
      );

      weatherCubit.emit(initialWeatherState);

      weatherCubit.toggleUnits();

      expect(
        weatherCubit.state.temperatureUnits,
        TemperatureUnits.fahrenheit,
      );
    });
    //

    test('calculateAverageTemp returns correct values', () {
      var icon = '10n';
      final testForecast = [
        Weather(
          temp: Temperature(value: 20),
          feelsLike: Temperature(value: 15),
          humidity: 50,
          pressure: 1100,
          windSpeed: 15,
          icon: icon,
          date: fakeDate,
          weatherCondition: weatherCondition,
        ),
        Weather(
          temp: Temperature(value: 30),
          feelsLike: Temperature(value: 25),
          humidity: 70,
          pressure: 1200,
          windSpeed: 25,
          icon: icon,
          date: fakeDate,
          weatherCondition: weatherCondition,
        ),
      ];

      final expectedAverageTemp = Temperature(value: 25);
      final expectedAverageFeelsLikeTemp = Temperature(value: 20);
      final avgWindSpeed = 20;
      final avgPressure = 1150;
      final avgHumidity = 60;

      final result = weatherCubit.calculateAverageTemp(testForecast);

      expect(result!.temp, expectedAverageTemp);
      expect(result.feelsLike, expectedAverageFeelsLikeTemp);
      expect(result.windSpeed, avgWindSpeed);
      expect(result.pressure, avgPressure);
      expect(result.humidity, avgHumidity);
      expect(result.weatherCondition, weatherCondition);
      expect(result.date, fakeDate);
      expect(result.icon, icon);
    });
  });
}
