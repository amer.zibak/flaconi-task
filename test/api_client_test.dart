import 'package:flaconi_task/api_models/weather.dart';
import 'package:flaconi_task/api_models/weather_condition.dart';
import 'package:flaconi_task/api_models/weather_data.dart';
import 'package:flaconi_task/api_models/weather_wind.dart';
import 'package:flaconi_task/client_api/rest_client.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'api_client_test.mocks.dart';

@GenerateMocks([RestClient])
void main() {
  group("#fetchWeatherData", () {
    late RestClient restClient;
    setUp(() {
      restClient = MockRestClient(); // #1
    });

    test('Fetch WeatherData Successful Call', () async {
      final weatherCondition = generateRandomWeatherCondition();
      final weatherWind = generateRandomWeatherWind();
      final time = DateTime.now();
      final randomListOfWeatherData = generateRandomWeatherData();
      when(restClient.getWeather(22.2, 33.3)).thenAnswer(// #2
          (realInvocation) async => WeatherData(
              main: randomListOfWeatherData,
              weather: weatherCondition,
              dateTime: time,
              wind: weatherWind));

      final expected = WeatherData(
          main: randomListOfWeatherData,
          weather: weatherCondition,
          dateTime: time,
          wind: weatherWind);

      expect(await restClient.getWeather(22.2, 33.3), expected);
    });
  });
}

Weather generateRandomWeatherData() {
  return const Weather(
    temp: 23.3,
    feelsLike: 22.1,
    humidity: 2,
    pressure: 12,
  );
}

WeatherWind generateRandomWeatherWind() {
  return const WeatherWind(speed: 21.2);
}

List<WeatherCondition> generateRandomWeatherCondition() {
  return [
    const WeatherCondition(icon: '10d', main: 'Clear')];
}
