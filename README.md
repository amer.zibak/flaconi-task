# Weather case study task

Weather project task for a test case study.

## Getting Started

## Features

- support Celsius and Fahrenheit for feels like and temperature.
- support Portrait/Landscape screen mode.
- customize weather forecast city by simply edit config file.
- select a day from list to view details.
- simple design.

Weather forecast for 5 days in a given city, you can change the city in config/constants (
latCoordinate , lonCoordinate , cityNameCoordinate)
Source of Forecast is : https://openweathermap.org/forecast5

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
