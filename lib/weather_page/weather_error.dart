import 'package:flaconi_task/config/constants.dart';
import 'package:flutter/material.dart';

class WeatherError extends StatelessWidget {
  const WeatherError({required this.onRetry, super.key});

  final ValueGetter<Future<void>> onRetry;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('🙈', style: TextStyle(fontSize: 64)),
          Text(
            'Something went wrong!\nmaybe its a network error.',
            style: theme.textTheme.headlineSmall,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 24),
          Container(
            child: ElevatedButton(
              onPressed: onRetry,
              style: ElevatedButton.styleFrom(
                backgroundColor: Constants.darkBlueColor, //dark blue
              ),
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Icons.refresh),
                    SizedBox(width: 8),
                    Text('Retry'),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
