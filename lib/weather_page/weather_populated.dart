import 'package:cached_network_image/cached_network_image.dart';
import 'package:flaconi_task/config/constants.dart';
import 'package:flaconi_task/cubit/weather_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../models/weather.dart';

class WeatherPopulated extends StatelessWidget {
  const WeatherPopulated({
    required this.weather,
    required this.units,
    required this.onRefresh,
    required this.weatherForecast,
    required this.onDaySelected,
    super.key,
  });

  final Weather weather;
  final Map<DateTime, List<Weather>>? weatherForecast;
  final TemperatureUnits units;
  final ValueGetter<Future<void>> onRefresh;
  final Function(DateTime) onDaySelected;

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

    final theme = Theme.of(context);
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: isPortrait
          ? Column(
              children: weatherWidgets(theme, isPortrait),
            )
          : Row(
              children: weatherWidgets(theme, isPortrait),
            ),
    );
  }

  weatherWidgets(theme, bool isPortrait) => [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListView(
              children: [
                Center(
                  child: Text(
                    '${Constants.cityNameCoordinate} Weather',
                    style: theme.textTheme.displaySmall?.copyWith(
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                SizedBox(height: 8),
                Center(
                  child: Text(
                    // Format to get the day of the week
                    DateFormat('EEEE').format(weather.date),
                    style: theme.textTheme.titleLarge?.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Text(
                  weather.weatherCondition,
                  style: theme.textTheme.titleLarge?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          _WeatherIcon(condition: weather.icon),
                          Text(weather.formattedTemperature(units),
                              style: theme.textTheme.displayMedium),
                          SizedBox(height: 8),
                        ]),
                  ),
                ),
                iconWithText(
                  Icons.sunny_snowing,
                  weather.formattedFeelsLikeTemperature(units),
                  theme,
                ),
                iconWithText(
                  Icons.water_drop,
                  weather.formattedHumidity(),
                  theme,
                ),
                iconWithText(
                  Icons.compress,
                  weather.formattedPressure(),
                  theme,
                ),
                iconWithText(Icons.wind_power, weather.formattedWind(), theme),
                BlocBuilder<WeatherCubit, WeatherState>(
                  buildWhen: (previous, current) =>
                      previous.temperatureUnits != current.temperatureUnits,
                  builder: (context, state) {
                    return InkWell(
                      onTap: context.read<WeatherCubit>().toggleUnits,
                      child: ListTile(
                        title: const Text('Enable to use celsius (C).'),
                        isThreeLine: true,
                        subtitle: const Text(
                          'Temperature Units C/F',
                        ),
                        trailing: Switch(
                          value: state.temperatureUnits.isCelsius,
                          onChanged: (_) =>
                              context.read<WeatherCubit>().toggleUnits(),
                          activeColor: Colors.black,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: SizedBox(
              width: isPortrait ? null : 150,
              height: isPortrait ? 150 : null,
              child: ListView(
                  // itemCount: 5,
                  scrollDirection: isPortrait ? Axis.horizontal : Axis.vertical,
                  children: [
                    for (var day in weatherForecast!.keys)
                      InkWell(
                          onTap: () => onDaySelected(day),
                          child: _WeatherWeatherItem(
                              day, weatherForecast?[day] ?? [], units))
                  ])),
        ),
      ];

  Widget iconWithText(IconData icon, String text, theme) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Icon(icon),
          ),
          Text(
            text,
            style: theme.textTheme.bodyMedium,
          ),
        ],
      );
}

class _WeatherWeatherItem extends StatelessWidget {
  final DateTime date;
  final List<Weather> forecast;

  final TemperatureUnits units;

  const _WeatherWeatherItem(
    this.date,
    this.forecast,
    this.units,
  );

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        // Adjust the radius as per your requirement
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
          ),
        ],
      ),
      child: Column(
        children: [
          Text(DateFormat('EEEE').format(date)),
          _WeatherIcon(
            condition: forecast.first.icon,
          ),
          Text(
            formattedMinMaxTemperature(forecast, units),
            style: theme.textTheme.bodyMedium,
          ),
        ],
      ),
    );
  }

  String formattedMinMaxTemperature(
      List<Weather> list, TemperatureUnits units) {
    var min = list.map((e) => e.temp.value).reduce((a, b) => a < b ? a : b);
    var minTemp = formatTemperatureByUnit(min, units, showUnitLabel: false);
    var max = list.map((e) => e.temp.value).reduce((a, b) => a > b ? a : b);
    var maxTemp = formatTemperatureByUnit(max, units);
    return '$minTemp/$maxTemp';
  }
}

class _WeatherIcon extends StatelessWidget {
  const _WeatherIcon({required this.condition});

  static const _iconSize = 75.0;

  final String condition;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: 'https://openweathermap.org/img/wn/$condition@4x.png',
      height: _iconSize,
    );
  }
}

extension on Weather {
  String formattedTemperature(TemperatureUnits units) {
    return formatTemperatureByUnit(temp.value, units);
  }

  String formattedFeelsLikeTemperature(TemperatureUnits units) {
    return 'Feels Like: ${formatTemperatureByUnit(feelsLike.value, units)}';
  }

  String formattedHumidity() {
    return 'Humidity: $humidity%';
  }

  String formattedWind() {
    return 'Wind $windSpeed km/h';
  }

  String formattedPressure() {
    return 'Pressure: $pressure hPa';
  }
}

String formatTemperatureByUnit(double value, TemperatureUnits units,
    {bool showUnitLabel = true}) {
  return units.isCelsius
      ? ('${value.toInt()}°${showUnitLabel ? 'C' : ''}')
      : '${value.toFahrenheit().toInt()}°${showUnitLabel ? 'F' : ''}';
}
