import 'package:flaconi_task/cubit/weather_cubit.dart';
import 'package:flaconi_task/repository/weather_repository.dart';
import 'package:flaconi_task/weather_page/weather_error.dart';
import 'package:flaconi_task/weather_page/weather_loading.dart';
import 'package:flaconi_task/weather_page/weather_populated.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => WeatherCubit(context.read<WeatherRepository>()),
      child: const WeatherView(),
    );
  }
}

class WeatherView extends StatefulWidget {
  const WeatherView({super.key});

  @override
  State<WeatherView> createState() => _WeatherViewState();
}

class _WeatherViewState extends State<WeatherView> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WeatherCubit, WeatherState>(
      listener: (context, state) {
        print('listener changed $state');
      },
      builder: (context, state) {
        debugPrint('state updated ' + state.toString());
        switch (state.status) {
          case WeatherStatus.initial:
            context.read<WeatherCubit>().fetchWeather();
            return const WeatherLoading();
          case WeatherStatus.loading:
            return const WeatherLoading();
          case WeatherStatus.success:
            return WeatherPopulated(
              weather: state.weather,
              weatherForecast: state.forecast,
              units: state.temperatureUnits,
              onRefresh: () {
                return context.read<WeatherCubit>().refreshWeather();
              },
              onDaySelected: (day) {
                return context.read<WeatherCubit>().selectDay(day);
              },
            );
          case WeatherStatus.failure:
            return WeatherError(onRetry: () {
              return context.read<WeatherCubit>().refreshWeather();
            });
        }
      },
    );
  }
}
