import 'package:cached_network_image/cached_network_image.dart';
import 'package:flaconi_task/config/constants.dart';
import 'package:flaconi_task/weather_page/weather_page.dart';
import 'package:flutter/material.dart';

class RootWeatherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SafeArea(
            child: CachedNetworkImage(
                height: 70,
                imageUrl:
                    // I couldn't find a high quality PNG image for Flaconi logo.
                    "https://cdn.flaconi.de/media/email/f_logo_resized.jpg"),
          ),
          Expanded(
            child: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.white,
                      Colors.white,
                      Constants.blueSkyColor, // Blue sky
                    ],
                  ),
                ),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: WeatherPage(),
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
