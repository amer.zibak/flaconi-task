import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather.g.dart';

@JsonSerializable()
class Weather extends Equatable {
  const Weather({
    required this.temp,
    required this.feelsLike,
    required this.humidity,
    required this.pressure,
  });

  final double temp;
  final double feelsLike;
  final int humidity, pressure;

  factory Weather.fromJson(Map<String, dynamic> json) =>
      _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);

  @override
  List<Object> get props => [temp, feelsLike, humidity, pressure];
}
