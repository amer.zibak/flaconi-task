import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_condition.g.dart';

@JsonSerializable()
class WeatherCondition extends Equatable {
  const WeatherCondition({
    required this.icon,
    required this.main,
  });

  final String icon, main;

  factory WeatherCondition.fromJson(Map<String, dynamic> json) =>
      _$WeatherConditionFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherConditionToJson(this);

  @override
  List<Object> get props => [icon, main];
}
