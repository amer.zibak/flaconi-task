// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherData _$WeatherDataFromJson(Map<String, dynamic> json) => $checkedCreate(
      'WeatherData',
      json,
      ($checkedConvert) {
        final val = WeatherData(
          main: $checkedConvert(
              'main', (v) => Weather.fromJson(v as Map<String, dynamic>)),
          weather: $checkedConvert(
              'weather',
              (v) => (v as List<dynamic>)
                  .map((e) =>
                      WeatherCondition.fromJson(e as Map<String, dynamic>))
                  .toList()),
          wind: $checkedConvert(
              'wind', (v) => WeatherWind.fromJson(v as Map<String, dynamic>)),
          dateTime:
              $checkedConvert('dt_txt', (v) => DateTime.parse(v as String)),
        );
        return val;
      },
      fieldKeyMap: const {'dateTime': 'dt_txt'},
    );

Map<String, dynamic> _$WeatherDataToJson(WeatherData instance) =>
    <String, dynamic>{
      'main': instance.main,
      'weather': instance.weather,
      'wind': instance.wind,
      'dt_txt': instance.dateTime.toIso8601String(),
    };
