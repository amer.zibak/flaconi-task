// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_condition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherCondition _$WeatherConditionFromJson(Map<String, dynamic> json) =>
    $checkedCreate(
      'WeatherCondition',
      json,
      ($checkedConvert) {
        final val = WeatherCondition(
          icon: $checkedConvert('icon', (v) => v as String),
          main: $checkedConvert('main', (v) => v as String),
        );
        return val;
      },
    );

Map<String, dynamic> _$WeatherConditionToJson(WeatherCondition instance) =>
    <String, dynamic>{
      'icon': instance.icon,
      'main': instance.main,
    };
