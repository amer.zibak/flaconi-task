// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_forecast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherForecast _$WeatherForecastFromJson(Map<String, dynamic> json) =>
    $checkedCreate(
      'WeatherForecast',
      json,
      ($checkedConvert) {
        final val = WeatherForecast(
          list: $checkedConvert(
              'list',
              (v) => (v as List<dynamic>)
                  .map((e) => WeatherData.fromJson(e as Map<String, dynamic>))
                  .toList()),
        );
        return val;
      },
    );

Map<String, dynamic> _$WeatherForecastToJson(WeatherForecast instance) =>
    <String, dynamic>{
      'list': instance.list,
    };
