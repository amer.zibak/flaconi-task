// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_wind.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherWind _$WeatherWindFromJson(Map<String, dynamic> json) => $checkedCreate(
      'WeatherWind',
      json,
      ($checkedConvert) {
        final val = WeatherWind(
          speed: $checkedConvert('speed', (v) => (v as num).toDouble()),
        );
        return val;
      },
    );

Map<String, dynamic> _$WeatherWindToJson(WeatherWind instance) =>
    <String, dynamic>{
      'speed': instance.speed,
    };
