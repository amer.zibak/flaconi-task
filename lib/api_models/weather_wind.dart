import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_wind.g.dart';

@JsonSerializable()
class WeatherWind extends Equatable {
  const WeatherWind({
    required this.speed,
  });

  final double speed;

  factory WeatherWind.fromJson(Map<String, dynamic> json) =>
      _$WeatherWindFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherWindToJson(this);

  @override
  List<Object> get props => [speed];
}
