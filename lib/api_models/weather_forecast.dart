import 'package:equatable/equatable.dart';
import 'package:flaconi_task/api_models/weather_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_forecast.g.dart';

@JsonSerializable()
class WeatherForecast extends Equatable {
  const WeatherForecast({
    required this.list,
  });

  final List<WeatherData> list;

  factory WeatherForecast.fromJson(Map<String, dynamic> json) =>
      _$WeatherForecastFromJson(json);

  @override
  List<Object> get props => [list];
}
