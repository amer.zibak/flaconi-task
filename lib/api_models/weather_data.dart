import 'package:equatable/equatable.dart';
import 'package:flaconi_task/api_models/weather.dart';
import 'package:flaconi_task/api_models/weather_condition.dart';
import 'package:flaconi_task/api_models/weather_wind.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_data.g.dart';

@JsonSerializable()
class WeatherData extends Equatable {
  const WeatherData({
    required this.main,
    required this.weather,
    required this.wind,
    required this.dateTime,
  });

  final Weather main;

  final List<WeatherCondition> weather;
  final WeatherWind wind;

  @JsonKey(name: 'dt_txt')
  final DateTime dateTime;

  factory WeatherData.fromJson(Map<String, dynamic> json) =>
      _$WeatherDataFromJson(json);

  @override
  List<Object> get props => [main, weather, dateTime];
}
