import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart'; // Import the collection package
import 'package:equatable/equatable.dart';
import 'package:flaconi_task/repository/weather_repository.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../models/weather.dart';

part 'weather_cubit.g.dart';
part 'weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  WeatherCubit(this._weatherRepository) : super(WeatherState());

  final WeatherRepository _weatherRepository;

  Future<void> fetchWeather() async {
    debugPrint('fetching weather data');
    emit(state.copyWith(status: WeatherStatus.loading));

    try {
      final weatherWithForecast = await _weatherRepository.getForecast();
      final weather = weatherWithForecast.forecast[0];

      //we will group forecast items by date, to get daily forecast, regardless hourly forecast
      Map<DateTime, List<Weather>>? forecast =
          groupBy(weatherWithForecast.forecast, (p0) => p0.date);

      emit(
        state.copyWith(
          status: WeatherStatus.success,
          weather: weather,
          forecast: forecast,
        ),
      );
    } catch (e) {
      print(e.toString());
      emit(state.copyWith(status: WeatherStatus.failure));
    }
  }

  Future<void> refreshWeather() async {
    return fetchWeather();
  }

  void toggleUnits({bool toggleCurrentValue = false}) {
    final units = !toggleCurrentValue && state.temperatureUnits.isFahrenheit
        ? TemperatureUnits.celsius
        : TemperatureUnits.fahrenheit;

    //in case user toggle units before getting data from api
    if (!state.status.isSuccess) {
      emit(state.copyWith(temperatureUnits: units));
      return;
    }

    final weather = state.weather;
    emit(
      state.copyWith(
        temperatureUnits: units,
        weather: weather,
      ),
    );
  }

  @override
  WeatherState fromJson(Map<String, dynamic> json) =>
      WeatherState.fromJson(json);

  @override
  Map<String, dynamic> toJson(WeatherState state) => state.toJson();

  selectDay(DateTime day) {
    final weather = calculateAverageTemp(state.forecast![day]!);

    emit(
      state.copyWith(weather: weather),
    );
  }

  /// calculate average values based on daily 3 hours forecast,
  /// @return single weather object to show as today forecast
  Weather? calculateAverageTemp(List<Weather> forcast) {
    if (forcast.isEmpty) return null;

    // Sum up the values
    double tempTotal = 0,
        feelsLikeTotal = 0,
        windTotal = 0,
        humidityTotal = 0,
        pressureTotal = 0;
    // we will calculate average values in a single loop
    for (var obj in forcast) {
      tempTotal += obj.temp.value;
      feelsLikeTotal += obj.feelsLike.value;
      humidityTotal += obj.humidity;
      pressureTotal += obj.pressure;
      windTotal += obj.windSpeed;
    }

    // Calculate the average
    double average = tempTotal / forcast.length;
    double averageFeelsLike = feelsLikeTotal / forcast.length;
    double averageWind = windTotal / forcast.length;
    double averageHumidity = humidityTotal / forcast.length;
    double averagePressure = pressureTotal / forcast.length;
    // we will copy first item because it has primary weather condition and other values
    return forcast[0].copyWith(
      temp: Temperature(value: average),
      feelsLike: Temperature(value: averageFeelsLike),
      pressure: averagePressure.toInt(),
      humidity: averageHumidity.toInt(),
      windSpeed: averageWind.toInt(),
    );
  }
}

extension TempratureUnitsCalculation on double {
  double toFahrenheit() => (this * 9 / 5) + 32;
}
