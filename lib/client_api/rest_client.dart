import 'package:dio/dio.dart';
import 'package:flaconi_task/api_models/weather_data.dart';
import 'package:flaconi_task/api_models/weather_forecast.dart';
import 'package:flaconi_task/config/constants.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: 'https://api.openweathermap.org/data/2.5/')
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET('forecast')
  Future<WeatherForecast> getForecast(
    @Query('lat') double lat,
    @Query('lon') double lon, {
    @Query('appid') String appid = Constants.weatherAppId,
    @Query('units') String? units = 'metric',
  });

  @GET('weather')
  Future<WeatherData> getWeather(
    @Query('lat') double lat,
    @Query('lon') double lon, {
    @Query('appid') String appid = Constants.weatherAppId,
    @Query('units') String? units = 'metric',
  });
}
