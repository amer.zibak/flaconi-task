import 'package:dio/dio.dart';
import 'package:flaconi_task/client_api/rest_client.dart';
import 'package:flaconi_task/config/constants.dart';
import 'package:flaconi_task/models/weather_forecast.dart';

import '../models/weather.dart';

class WeatherRepository {
  WeatherRepository({RestClient? weatherApiClient})
      : _weatherApiClient = weatherApiClient ?? RestClient(Dio());

  final RestClient _weatherApiClient;

  Future<WeatherForecast> getForecast() async {
    final forecast = await _weatherApiClient.getForecast(
      Constants.latCoordinate,
      Constants.lonCoordinate,
    );

    final List<Weather> list = List.empty(growable: true);

    for (var weather in forecast.list) {
      list.add(Weather(
        feelsLike: Temperature(value: weather.main.feelsLike),
        temp: Temperature(value: weather.main.temp),
        humidity: weather.main.humidity,
        pressure: weather.main.pressure,
        icon: weather.weather[0].icon,
        weatherCondition: weather.weather[0].main,
        windSpeed: weather.wind.speed.toInt(),
        // we set hour to 0 to group items by date easily, we don't need hours forecast
        date: weather.dateTime.copyWith(
            hour: 0, minute: 0, second: 0, microsecond: 0, millisecond: 0),
      ));
    }

    return WeatherForecast(forecast: list);
  }

/*
/// get weather by '/weather' api
Future<Weather> getWeather() async {
  debugPrint('fetching weather data');
  final weather = await _weatherApiClient.getWeather(
    Constants.berlinLatCoordinate,
    Constants.berlinLonCoordinate,
  );

  final now = Weather(
    feelsLike: Temperature(value: weather.main.feelsLike),
    temp: Temperature(value: weather.main.temp),
    humidity: weather.main.humidity,
    pressure: weather.main.pressure,
    icon: weather.weather[0].icon,
    weatherCondition: weather.weather[0].main,
    windSpeed: weather.wind.speed.toInt(),
  );
  return now;
}*/
}
