import 'dart:ui';

class Constants {
  static const double latCoordinate = 52.5200;
  static const double lonCoordinate = 13.4050;
  static const String cityNameCoordinate = 'Berlin';

  static const weatherAppId = 'e5dd242e023861407fbefb01166ffd56';

  /// Units of measurement. "standard", "metric" and "imperial" units are available.
  /// If you do not use the units parameter, standard units will be applied by default.
  static const weatherApiUnits = 'metric';

  static const blueSkyColor = Color(0xFF6AAADC);
  static const darkBlueColor = Color(0xFF09123D);
}
