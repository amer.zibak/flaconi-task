import 'package:equatable/equatable.dart';
import 'package:flaconi_task/models/weather.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_forecast.g.dart';

@JsonSerializable()
class WeatherForecast extends Equatable {
  final List<Weather> forecast;

  const WeatherForecast({
    required this.forecast,
  });

  factory WeatherForecast.fromJson(Map<String, dynamic> json) =>
      _$WeatherForecastFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherForecastToJson(this);

  static final empty = const WeatherForecast(forecast: []);

  WeatherForecast copyWith(
      {List<Weather>? forecast, Weather? weatherNow, DateTime? lastUpdate}) {
    return WeatherForecast(
      forecast: forecast ?? this.forecast,
    );
  }

  @override
  List<Object> get props => [forecast];
}
