import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather.g.dart';

enum TemperatureUnits { fahrenheit, celsius }

extension TemperatureUnitsX on TemperatureUnits {
  bool get isFahrenheit => this == TemperatureUnits.fahrenheit;

  bool get isCelsius => this == TemperatureUnits.celsius;
}

@JsonSerializable()
class Temperature extends Equatable {
  const Temperature({required this.value});

  factory Temperature.fromJson(Map<String, dynamic> json) =>
      _$TemperatureFromJson(json);

  final double value;

  Map<String, dynamic> toJson() => _$TemperatureToJson(this);

  @override
  List<Object> get props => [value];
}

@JsonSerializable()
class Weather extends Equatable {
  final String icon, weatherCondition;
  final int windSpeed;
  final DateTime date;

  const Weather({
    required this.temp,
    required this.feelsLike,
    required this.humidity,
    required this.pressure,
    required this.icon,
    required this.windSpeed,
    required this.weatherCondition,
    required this.date,
  });

  final Temperature temp;
  final Temperature feelsLike;
  final int pressure, humidity;

  factory Weather.fromJson(Map<String, dynamic> json) =>
      _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);

  static final empty = Weather(
    temp: const Temperature(value: 0),
    feelsLike: const Temperature(value: 0),
    humidity: 0,
    pressure: 0,
    icon: '',
    windSpeed: 0,
    weatherCondition: '',
    date: DateTime.now(),
  );

  Weather copyWith({
    DateTime? lastUpdated,
    Temperature? temp,
    Temperature? feelsLike,
    int? humidity,
    int? pressure,
    String? icon,
    int? windSpeed,
    String? weatherCondition,
    DateTime? date,
  }) {
    return Weather(
      temp: temp ?? this.temp,
      feelsLike: feelsLike ?? this.feelsLike,
      humidity: humidity ?? this.humidity,
      pressure: pressure ?? this.pressure,
      icon: icon ?? this.icon,
      windSpeed: windSpeed ?? this.windSpeed.toInt(),
      weatherCondition: weatherCondition ?? this.weatherCondition,
      date: date ?? this.date,
    );
  }

  @override
  List<Object> get props => [temp, feelsLike, humidity, pressure];
}
