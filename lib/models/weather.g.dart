// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Temperature _$TemperatureFromJson(Map<String, dynamic> json) => $checkedCreate(
      'Temperature',
      json,
      ($checkedConvert) {
        final val = Temperature(
          value: $checkedConvert('value', (v) => (v as num).toDouble()),
        );
        return val;
      },
    );

Map<String, dynamic> _$TemperatureToJson(Temperature instance) =>
    <String, dynamic>{
      'value': instance.value,
    };

Weather _$WeatherFromJson(Map<String, dynamic> json) => $checkedCreate(
      'Weather',
      json,
      ($checkedConvert) {
        final val = Weather(
          temp: $checkedConvert(
              'temp', (v) => Temperature.fromJson(v as Map<String, dynamic>)),
          feelsLike: $checkedConvert('feels_like',
              (v) => Temperature.fromJson(v as Map<String, dynamic>)),
          humidity: $checkedConvert('humidity', (v) => v as int),
          pressure: $checkedConvert('pressure', (v) => v as int),
          icon: $checkedConvert('icon', (v) => v as String),
          windSpeed: $checkedConvert('wind_speed', (v) => v as int),
          weatherCondition:
              $checkedConvert('weather_condition', (v) => v as String),
          date: $checkedConvert('date', (v) => DateTime.parse(v as String)),
        );
        return val;
      },
      fieldKeyMap: const {
        'feelsLike': 'feels_like',
        'windSpeed': 'wind_speed',
        'weatherCondition': 'weather_condition'
      },
    );

Map<String, dynamic> _$WeatherToJson(Weather instance) => <String, dynamic>{
      'icon': instance.icon,
      'weather_condition': instance.weatherCondition,
      'wind_speed': instance.windSpeed,
      'date': instance.date.toIso8601String(),
      'temp': instance.temp,
      'feels_like': instance.feelsLike,
      'pressure': instance.pressure,
      'humidity': instance.humidity,
    };
