// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_forecast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherForecast _$WeatherForecastFromJson(Map<String, dynamic> json) =>
    $checkedCreate(
      'WeatherForecast',
      json,
      ($checkedConvert) {
        final val = WeatherForecast(
          forecast: $checkedConvert(
              'forecast',
              (v) => (v as List<dynamic>)
                  .map((e) => Weather.fromJson(e as Map<String, dynamic>))
                  .toList()),
        );
        return val;
      },
    );

Map<String, dynamic> _$WeatherForecastToJson(WeatherForecast instance) =>
    <String, dynamic>{
      'forecast': instance.forecast,
    };
